sudo apt-get update -y > /dev/null 2>&1

sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $cf_mysql_rootpwd"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $cf_mysql_rootpwd"
sudo apt-get install -y mysql-client mysql-server > /dev/null 2>&1
mysql -uroot -p$cf_mysql_rootpwd -e "CREATE DATABASE $cf_db_name;"
mysql -uroot -p$cf_mysql_rootpwd -e "GRANT ALL PRIVILEGES ON $cf_db_name.* TO $cf_db_user@localhost IDENTIFIED BY '$cf_db_user_pwd' WITH GRANT OPTION;"
mysql -uroot -p$cf_mysql_rootpwd -e "GRANT ALL PRIVILEGES ON $cf_db_name.* TO $cf_db_user@\"%\" IDENTIFIED BY '$cf_db_user_pwd' WITH GRANT OPTION;"
sudo service mysql restart

sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt -y
sudo php5enmod mcrypt

echo "ServerName localhost" | sudo tee /etc/apache2/conf-available/servername.conf
sudo a2enconf servername
sudo service apache2 start
echo -e "\n--- Setting virtualhost for ${cf_hostname} ---\n"
VHOST=$(cat <<EOF
<VirtualHost *:80>
	ServerName ${cf_hostname}
	ServerAdmin admin@${cf_hostname}
	DocumentRoot "${cf_source_target_dir}/app/webroot"
	ErrorLog  \${APACHE_LOG_DIR}/${cf_hostname}-error.log
	CustomLog \${APACHE_LOG_DIR}/${cf_hostname}-access.log combined
	<Directory "${cf_source_target_dir}/app/webroot">
		Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
	</Directory>
	<Directory /var/www/>
	    Options FollowSymLinks
	    AllowOverride All
	    Require all granted
	</Directory>
</VirtualHost>
EOF
)
echo "${VHOST}" | sudo tee /etc/apache2/sites-available/000-default.conf
sudo a2enmod rewrite

# restart apache
sudo service apache2 restart

# install Composer
curl -s https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer